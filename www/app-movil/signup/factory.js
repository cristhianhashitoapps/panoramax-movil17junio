angular
  .module('app-movil')
  .factory('SignUpFactory', [
    '$http',
    '$q',
    '$urlBaseApi',
    function($http, $q, $url) {
      var urlBase = $url.Url();
      var api = {};

      api.SignUp = function(usuario) {
        // return $q(function(resolve) {
        //   resolve({data: []});
        // });

        return $http.post([urlBase, 'SignUp', ''].join('/'), usuario);
      };

      api.Update = function(telefono, usuario) {
        // return $q(function(resolve) {
        //   resolve({data: []});
        // });

        return $http.put([urlBase,'Doctor',telefono].join('/'), usuario);
      };

      api.LoadAddress = function(telefono) {
        // return $q(function(resolve) {
        //   resolve({data: []});
        // });

        return $http.get([urlBase,'Doctor',telefono,'Empresa'].join('/'));
      };

      api.UpdateAddress = function(Id, direccion) {
        // return $q(function(resolve) {
        //   resolve({data: []});
        // });

        return $http.put([urlBase,'Empresa',Id].join('/'), direccion);
      };

      api.InsertAddress = function(direccion) {
        // return $q(function(resolve) {
        //   resolve({data: []});
        // });

        return $http.post([urlBase,'Empresa'].join('/'), direccion);
      };

      api.DeleteAddress = function(Id) {
        // return $q(function(resolve) {
        //   resolve({data: []});
        // });

        return $http.delete([urlBase,'Empresa',  Id
        ].join('/'));
      };

      api.GetOrders = function(Telefono) {
        return $http.get([
          urlBase,
          'usuarios',
          Telefono,
          'pedidos'
        ].join('/'));
      };

      //obtener pedidos panoramax
      api.GetOrdersPanoramax = function(Id) {
        return $http.get([
          urlBase,
          'Doctor',
          Id,
          'GetOrdersPanoramax'
        ].join('/'));
      };

      //inserta paciente panoramax
      api.InsertPaciente = function(paciente) {
              return $http.post([urlBase,'Paciente'].join('/'), paciente);
      };

      //inserta paciente panoramax
      api.CargarDetallesPorSolicitudExamen = function(Id) {
              return $http.get([urlBase,'DetallesExamenes',Id,'CargarDetallesPorSolicitudExamen'].join('/'), Id);
      };

      return api;
    }
  ]);
