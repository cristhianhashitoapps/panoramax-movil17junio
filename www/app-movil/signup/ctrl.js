angular
  .module('app-movil')
  .controller('SignUpCtrl', ['$scope', '$auth', '$state', 'SignUpFactory',
    function($scope, $auth, $state, SignUpFactory) {
      $scope.loginData = {};
      $scope.position  = {};

      $scope.doLogin = function() {
        console.log($scope.loginData);

        if ($scope.loginData.Terminos) {
          SignUpFactory
            .SignUp($scope.loginData)
            .then(function(result) {
              $scope.$parent.user = result.data;
              console.log(result);
              console.log("user"+$scope.$parent.user.clave);
              $state.go('validar');
            })
            .catch(function(error) {
              console.log(error);
            });
        } else {
          console.log('Debe de aceptar los terminos antes de continuar!!!');
        }
        console.log('validar');
      };
    }
  ])
  .controller('ValidarCtrl', ['$scope', '$auth', '$state',
    function($scope, $auth, $state) {

      $scope.loginData = {
        telefono: $scope.$parent.user.telefono
      };

      $scope.doValidate = function() {
        $scope.loginData.clave = $scope.loginData.clave.toUpperCase();

        $auth
          .login($scope.loginData)
          .then(function(result) {
            $state.go('app.index');
          })
          .catch(function(error) {
            console.log(error);
          });
      };
    }
  ])
  .controller('ProfileCtrl', [
    '$scope',
    '$auth',
    'SignUpFactory',
    '$ionicHistory',
    function($scope, $auth, SignUpFactory, $ionicHistory) {
      $scope.Empresa         = {};
      $scope.profileData       = $scope.$parent.user;
      $scope.mostrarFormulario = false;

      function UpdateAddressList() {
        SignUpFactory
          .LoadAddress($scope.profileData.Telefono)
          .then(function(result) {
            $scope.$parent.user.Empresa = result.data;

            $scope.Empresa         = {};
            $scope.mostrarFormulario = false;
          })
          .catch(function(error) {
            console.log(error);
          });
      }
      $scope.saveAddress = function() {
        var factory = {};

        console.log($scope.Empresa);

        if ($scope.Empresa.Id) {
          factory = SignUpFactory
            .UpdateAddress($scope.Empresa.Id, $scope.Empresa);
        } else {
          $scope.Empresa.Doctortelefono = $scope.profileData.Telefono;

          factory = SignUpFactory
            .InsertAddress($scope.Empresa);
        }

        factory
          .then(function(result) {
            UpdateAddressList();
          })
          .catch(function(error) {
            console.log(error);
          });
      };

      $scope.showForm = function(dir) {
        console.log(dir);
        console.log($scope.profileData.Empresa);

        if (dir) {
          $scope.Empresa = dir;
        } else {
          $scope.Empresa = {};
        }

        $scope.mostrarFormulario = true;

        console.log($scope.Empresa);
      };

      $scope.doUpdateProfile = function() {
        SignUpFactory
          .Update($scope.profileData.telefono, $scope.profileData)
          .then(function(result) {
            $scope.profileData = $scope.$parent.user = result.data;
            $auth.login($scope.$parent.user);

            UpdateAddressList();

            $ionicHistory.goBack();
          })
          .catch(function(error) {
            console.log(error);
          });
      };

      $scope.deleteDir = function(Id) {
        SignUpFactory
          .DeleteAddress(Id)
          .then(function(result) {
            UpdateAddressList();
          })
          .catch(function(error) {
            console.log(error);
          });
      };
    }]
  ).controller('OrderCtrl', [
    '$scope',
    '$state',
    '$ionicModal',
    '$ionicLoading',
    '$timeout',
    'SignUpFactory',
    'ProductFactory',
    function($scope, $state, $ionicModal, $ionicLoading, $timeout, SignUpFactory, ProductFactory) {
      $scope.pedidos      = [];
      $scope.profileData  = $scope.$parent.user;
      $scope.currentOrder = {};

      $state.go($state.current, {}, {reload: true});

      $ionicModal.fromTemplateUrl(
        'app-movil/home/detallePedido.html',
        {
          scope: $scope,
          animation: 'slide-in-up'
        }
      ).then(function(modal) {

        $scope.modal = modal;
      });

      function activate() {
        $ionicLoading.show({
          template: [
            '<ion-spinner></ion-spinner>',
            'Espera un momento,',
            'estamos cargando tus ultimos pedidos...'
          ].join('<br>')
        });
        SignUpFactory
          .GetOrdersPanoramax($scope.profileData.Id)
          .then(function(result) {
            $scope.pedidos = result.data;

            $timeout(function() {
              $ionicLoading.hide();
            }, 2500);
          })
          .catch(function(error) {
            console.log(error);
          });
      };

      activate();

      $scope.cerrarDetallePedido = function() {
        $scope.modal.hide();
      };

      $scope.VerDetalle = function(order) {
        console.log(order);

        SignUpFactory
          .CargarDetallesPorSolicitudExamen(order.Id)
          .then(function(result){
            //$scope.currentOrder = result.data;
            console.log("datos",result.data);

            //-------------------------
            //var data = $scope.checkscategorias.concat($scope.textcategorias);
            var data = result.data.map(function(item) {
              var result = item.SubCategorium;

              result.textocategoria  = item.campoAbierto || '';
              result.textocategoria  = result.textocategoria != 'ninguno' ? result.textocategoria : '';
              result.categorianombre = result.Categorium.nombre;

              return result;
            }).sort(function(a, b) {
              return a.CategoriumId - b.CategoriumId;
            });

            $scope.catShow = [];

            console.log(data);
            var clase = null;
            var showClase = null;
            var uniqueCategory = data
              .map(function(item) {
                return item.CategoriumId;
              })
              .filter(function(value, index, self) {
                return self.indexOf(value) === index;
              })
              .forEach(function(item) {
                var subCategoryItems = data.filter(function(sub) {
                  return sub.CategoriumId == item;
                }).sort(function(a, b) {
                  return a.Id - b.Id;
                }).forEach(function(sub, index) {
                  if (index == 0) {
                    $scope.catShow.push({
                      name: sub.categorianombre,
                      image: sub.ImagenPrincipal,
                      subs: []
                    });
                  }

                  showClase = clase != sub.clase;

                  if (clase != sub.clase) {
                    clase = sub.clase;
                  }

                  $scope.catShow[$scope.catShow.length - 1].subs.push({
                    name: sub.nombre,
                    clase: showClase ? sub.clase : null,
                    CssClass: sub.CssClass,
                    text: sub.textocategoria
                  });
                });
              });

            console.log($scope.catShow);

            //$scope.$apply();
            $scope.modal.show();
            //-------------------------


          })
          .catch(function(error){
            console.log(error);
          });

      };

      $scope.TotalCompra = function() {
        var total = 0;

        if ($scope.currentOrder && $scope.currentOrder.DetallePedidos) {
          [].forEach.call($scope.currentOrder.DetallePedidos, function(det) {
            total += det.Cantidad * det.Precio;
          });
        }

        return total;
      };

      $scope.CerrarOrden = function(order) {
        order.Estado = 'T';
        ProductFactory
          .UpdatePedido(order.Id, order)
          .then(function(result) {
            order = result.data;
          })
          .catch(function(error) {});
      };

      $scope.CancelarPedido = function(order) {
        order.Estado = 'R';
        ProductFactory
          .UpdatePedido(order.Id, order)
          .then(function(result) {
            order = result.data;
          })
          .catch(function(error) {});
      };
    }]
  );
