angular
  .module('app-movil', [
    'ionic',
    'ionic-pullup',
    'ionic.service.core',
    'ionic.service.push',
    'ngCordova',
    'ngDraggable',
    'satellizer',
    'urlBase',
    'ionic-toast'
  ]
);
