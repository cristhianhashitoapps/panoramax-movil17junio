angular
  .module('app-movil')
  .run(['$rootScope', '$state', '$stateParams', '$auth', 'ParametroFactory',
    function($rootScope, $state, $stateParams, $auth, ParametroFactory) {
      $rootScope.$state       = $state;
      $rootScope.$stateParams = $stateParams;
      $rootScope.$domicilio   = 0;

      /*ParametroFactory
        .GetValorDomicilio()
        .then(function(result) {
          $rootScope.$domicilio = result.data.Valor;
        })
        .catch(function(error) {
          console.log(error);
        });*/

      $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams) {
          console.log(toState);
          if (!$auth.isAuthenticated() &&
            toState.name != 'signup' &&
            toState.name != 'validar') {
            event.preventDefault();
            $state.go('signup');
          }
        }
      );
    }
  ])
  .config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('app', {
          url: '/app',
          abstract: true,
          templateUrl: 'app-movil/common/menu.html',
          controller: 'AppCtrl as main'
        })
        .state('signup', {
          url: '/signup',
          templateUrl: 'app-movil/signup/index.html',
          controller: 'SignUpCtrl'
        })
        .state('validar', {
          url: '/validar',
          templateUrl: 'app-movil/signup/validar.html',
          controller: 'ValidarCtrl',
          controllerAs: 'ctrl'
        })
        .state('app.index', {
          url: '/index',
          views: {
            'menuContent': {
              templateUrl: 'app-movil/home/index.html',
              controller: 'IndexCtrl'
            }
          }
        })
        .state('app.indexByCat', {
          url: '/index/:VariedadId',
          views: {
            'menuContent': {
              templateUrl: 'app-movil/home/index.html',
              controller: 'IndexCtrl'
            }
          }
        })
        .state('app.category', {
          url: '/index',
          views: {
            'menuContent': {
              templateUrl: 'app-movil/category/category.html',
              controller: 'IndexCtrl'
            }
          }
        })
        .state('app.profile', {
          url: '/profile',
          views: {
            'menuContent': {
              templateUrl: 'app-movil/signup/profile.html',
              controller: 'ProfileCtrl'
            }
          }
        })
        .state('app.orders', {
          url: '/orders',
          views: {
            'menuContent': {
              templateUrl: 'app-movil/signup/orders.html',
              controller: 'OrderCtrl'
            }
          }
        })
        .state('app.principal', {
          url: '/principal',
          views: {
            'menuContent': {
              templateUrl: 'app-movil/principal/index.html',
              controller: 'PrincipalCtrl'
            }
          }
        });

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/app/index');
    }]);
