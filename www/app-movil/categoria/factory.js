angular
  .module('app-movil')
  .factory('CategoriaFactory', CategoriaFactory);

//factory para las nuevos reqs de panoramax
CategoriaFactory.$inject = ['$http', '$q', '$urlBaseApi'];
function CategoriaFactory($http, $q, $url) {
  var urlBase = $url.Url();
  var api = {};

  api.GetCategoriasByTipoServicioId = function(tiposervicio_id) {
    return $http.get([urlBase, 'categorias',tiposervicio_id,'bytiposervicioid'].join('/'));
  };

  api.GetTipoServicio = function() {
    return $http.get([urlBase, 'tiposervicio'].join('/'));
  };

  //inserta el pedido de panoramax
  api.InsertPedidoPanoramax = function(pedido) {
    return $http.post([urlBase, 'SolicitudExamenes'].join('/'), pedido);
  };

  //inserta el detalle de cada item de panoramax
  api.InsertDetallePedidoPanoramax = function(detalle) {
    return $http.post([urlBase, 'DetallesExamenes','InsertarDetallePedido'].join('/'), detalle);
  };

  return api;
}
