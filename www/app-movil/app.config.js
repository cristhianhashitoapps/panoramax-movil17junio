angular
  .module('app-movil')
  .run(['$ionicPlatform', '$cordovaLocalNotification',
    function($ionicPlatform, $cordovaLocalNotification) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      // var socket = io.connect('ws://api-oxxo.inger.co', {'forceNew': true});

      // socket.on('notificacion', function(textoNotificacion) {
      //   console.log(textoNotificacion);
      //   console.log($cordovaLocalNotification);

      //   alert(textoNotificacion);

      //   $cordovaLocalNotification.schedule({
      //     id: 1,
      //     title: 'Oxxo App',
      //     text: textoNotificacion,
      //     data: {
      //       customProperty: 'custom value'
      //     }
      //   })
      //   .then(function(result) {
      //     console.log(result);
      //     alert(result);
      //   });
      // });
    });
  }])
  .config(['$authProvider', '$urlBaseApiProvider',
  function($authProvider, $url) {
    var urlBase = $url.Url();

    // Parametros de configuración autenticación
    $authProvider.loginUrl    = [urlBase, 'Doctor', 'login'].join('/');
    $authProvider.signupUrl   = [urlBase, 'Doctor', 'singup'].join('/');
    $authProvider.tokenName   = 'token';
    $authProvider.tokenPrefix = 'OxxoMovil';
  }],function($ionicCloudProvider) {
        $ionicCloudProvider.init({
          "core": {
            "app_id": "e36eff04"
          },
          "push": {
            "sender_id": "AIzaSyDDNv_NANxnbBx2A7Sr-7itlNLAUXQsLxg",
            "pluginConfig": {
              "ios": {
                "badge": true,
                "sound": true
              },
              "android": {
                "iconColor": "#343434"
              }
            }
          }
        })
      }
)
