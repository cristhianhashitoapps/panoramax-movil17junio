angular
  .module('app-movil')
  .controller('IndexCtrl', IndexCtrl);

IndexCtrl.$inject = [
  '$scope',
  '$auth',
  '$state',
  'CestaFactory',
  'ProductFactory',
  'MedioPagoFactory',
  'TipoEntregaResultadosFactory',
  'SignUpFactory',
  '$ionicModal',
  '$ionicSlideBoxDelegate',
  '$ionicLoading',
  '$timeout',
  '$ionicUser',
  '$ionicPush',
  'CategoriaFactory',
  'ionicToast'

];

function IndexCtrl($scope, $auth, $state,
CestaFactory, ProductFactory, MedioPagoFactory,
TipoEntregaResultadosFactory, SignUpFactory,
$ionicModal, $ionicSlideBoxDelegate, $ionicLoading,
$timeout, $ionicUser, $ionicPush,
CategoriaFactory,ionicToast) {
  $scope.products         = [];
  $scope.profileData      = $scope.$parent.user;
  $scope.direccionPedido  = null;
  $scope.medioPago        = null;
  $scope.categories       = {};
  $scope.mediosPago       = [];
  $scope.tiposEntrega     = [];
  $scope.checkscategorias = [];
  $scope.textcategorias   = [];
  $scope.catShow          = [];
  $scope.paciente         = {};
  $scope.textocategoria   = "";
  $scope.direcciones = [];
  $scope.observaciones="";

  //nuevo
  $scope.categorias = [];

  $ionicModal.fromTemplateUrl(
    'app-movil/products/detalleProducto.html',
    {
      scope: $scope,
      animation: 'slide-in-up'
    }
  ).then(function(modal) {

    $scope.modal = modal;
  });

  $ionicModal.fromTemplateUrl(
    'app-movil/home/detalleCesta.html',
    {
      scope: $scope,
      animation: 'slide-in-up'
    }
  ).then(function(modal) {

    $scope.modalCesta = modal;
  });

  $ionicModal.fromTemplateUrl(
    'app-movil/signup/nvaDireccion.html',
    {
      scope: $scope,
      animation: 'slide-in-up'
    }
  ).then(function(modal) {

    $scope.modalNvaDir = modal;
  });

  $ionicModal.fromTemplateUrl(
    'app-movil/signup/nvaPaciente.html',
    {
      scope: $scope,
      animation: 'slide-in-up'
    }
  ).then(function(modal) {

    $scope.modalNvaPaciente = modal;
  });

  function activate() {
    console.log($ionicUser);

    $scope.observaciones = '';

    $ionicLoading.show({
      template: [
        '<ion-spinner></ion-spinner>',
        'Espera por favor,',
        'estamos consultando nuestros productos...'
      ].join('<br>')
    });

    if ($state.params.VariedadId) {
      console.log("parametro: "+$state.params.VariedadId);
      $scope.categories.Id = $state.params.VariedadId;
    }

    TipoEntregaResultadosFactory
      .GetAllTipoEntregaResultados()
      .then(function(result) {
        $scope.tiposEntrega = result.data.rows;
      })
      .catch(function(error) {
        console.log(error);
      });
    /*ProductFactory
      .GetProductsByVariedad()
      .then(function(result) {
        console.log(result);
        $scope.products = result.data.rows;

        $ionicSlideBoxDelegate.update();

        $timeout(function() {
          $ionicLoading.hide();
        }, 2500);
      })
      .catch(function(error) {
        console.log(error);
      });*/

    CategoriaFactory
        .GetTipoServicio()
        .then(function(result){
          $scope.tiposervicios = result.data.rows;
          $ionicSlideBoxDelegate.update();

          $timeout(function() {
            $ionicLoading.hide();
          }, 2500);
        })
        .catch(function(error){
          console.log(error);
        });

    //cargar las direcciones
    console.log($scope.profileData);
    SignUpFactory
      .LoadAddress($scope.profileData.Id)
      .then(function(result) {

        $scope.direcciones = result.data;
        console.log("------direcciones-----");
        console.log($scope.direcciones);
        console.log("------direcciones-----");
      })
      .catch(function(error) {
        console.log(error);
      });

    console.log('$ionicPush');
    console.log($ionicPush);

    try {
      $ionicPush.init({
        debug: false,
        canShowAlert: true, //Se pueden mostrar alertas en pantalla
        canSetBadge: true, //Puede actualizar badgeds en la app
        canPlaySound: true, //Puede reproducir un sonido
        canRunActionsOnWake: true, //Puede ejecutar acciones fuera de la app
        onNotification: function(notification) {
          var payload = notification.payload;
          console.log(notification, payload);
          // alert(JSON.stringify(notification));
        },
        onRegister: function(data) {
          console.log(data.token);
          // alert(data.token);

          // this will give you a fresh user or the previously saved 'current user'
          var user = $ionicUser.current();

          // if the user doesn't have an id, you'll need to give it one.
          if (!user.id) {
            user.id    = $ionicUser.anonymousId();

            user.set('name', $scope.profileData.Nombre);
            user.set('phone', $scope.profileData.Telefono);
            user.addPushToken(data);
          }

          $scope.profileData.UserPushId   = user.id;
          $scope.profileData.DevicePushId = data.token;

          console.log("profileData-----------------------");
          console.log($scope.profileData);
          console.log("-----------------------");
          SignUpFactory
            .Update($scope.profileData.Id, $scope.profileData)
            .then(function(result) {})
            .catch(function(error) {});

          //persist the user
          user.save();
        },
      });

      /*$ionicPush.register().then(function(t) {
          return $ionicPush.saveToken(t);
      }).then(function(t) {
          console.log('Token saved:', t.token);
      }).catch(function(e){
          console.log('error',e);
      });

      $scope.$on('cloud:push:notification', function(event, data) {
        var msg = data.message;
        alert(msg.title + ': ' + msg.text);
      });*/



      $ionicPush.register();
    } catch (err) {
        console.log(err);
    }
  };

  activate();

  $scope.onDropComplete = function(data, evt) {
    CestaFactory.AddProduct(data, data.cant || 1);

    if ($scope.modal._isShown) {
      $scope.modal.hide();
    };

    data.cant = undefined;

    $scope.VerCesta();
  };

  $scope.ObtenerCantidadPedida = function(producto) {
    var prod = CestaFactory.ContainsProduct(producto);

    return prod ? prod.cnt : null;
  };

  $scope.verDetalleProducto = function(pro) {
    $scope.productoSeleccionado = pro;
    $scope.productoSeleccionado.cant = 1;

    //consultamos categorias por el tipo de servicio
    $scope.categoriasbytiposervicio=[];


    CategoriaFactory
      .GetCategoriasByTipoServicioId(pro.Id)
      .then(function (result){
        $scope.categoriasbytiposervicio = result.data["rows"];
        console.log(result);

        var aux = $scope.checkscategorias.concat($scope.textcategorias);

        console.log(aux);

        $scope.categoriasbytiposervicio.forEach(function(item) {
          item.selected = false;
          // aux.filter(function(ax) { return ax.CategoriumId == item.Id })
          //   .forEach(function(subs) {
          //     item.selected = item.selected || subs.campo == 'none';
          //   });

          item.SubCategoria.forEach(function(subs) {
            var selected = aux.filter(function(ax) { return ax.Id == subs.Id });

            item.selected = item.selected || (subs.campo == 'none' || subs.campo == 'check') && selected.length > 0;
            subs.selected = subs.campo == 'check' && selected.length > 0;
            subs.textocategoria = (subs.campo == 'texto'  && selected.length > 0) ? selected[0].textocategoria : '';
          });
        });

        $scope.modal.show();
      })
      .catch(function (error){
        console.log(error);
      });
  };

  $scope.cerrarDetalleProducto = function() {

    $scope.modal.hide();
  };

  $scope.VerCesta = function() {
    console.log("VerCesta");
    $scope.Productos = CestaFactory.GetItems();
    console.log($scope.Productos);
    console.log($scope.checkscategorias);

    var data = $scope.checkscategorias.concat($scope.textcategorias);
    
    $scope.catShow = [];

    data.sort(function(a, b) {
      return a.CategoriumId - b.CategoriumId;
    });

    var uniqueCategory = data
      .map(function(item) {
        return item.CategoriumId;
      })
      .filter(function(value, index, self) { 
        return self.indexOf(value) === index;
      })
      .forEach(function(item) {
        var subCategoryItems = data.filter(function(sub) {
          return sub.CategoriumId == item;
        }).sort(function(a, b) {
          return a.Id - b.Id;
        }).forEach(function(sub, index) {
          if (index == 0) {
            $scope.catShow.push({
              name: sub.categorianombre,
              image: sub.ImagenPrincipal,
              subs: []
            });
          }

          $scope.catShow[$scope.catShow.length - 1].subs.push({
            name: sub.nombre,
            CssClass: sub.CssClass,
            text: sub.textocategoria
          });
        });
      });

    console.log($scope.catShow);

    $scope.$apply();
  };

  $scope.cerrarDetalleCesta = function() {

    $scope.modalCesta.hide();
  };

  $scope.TotalCompra = function() {
    var total = 0;

    [].forEach.call(CestaFactory.GetItems(), function(pro) {
      if (pro.cnt) {
        total = total + pro.cnt * pro.Precio;
      }
    });

    return total;
  };

  $scope.VaciarCanasta = function() {
    CestaFactory.Clear();

    $scope.cerrarDetalleCesta();

    $scope.VerCesta();
  };

  $scope.VaciarPedidoPanoramax = function(){
    $scope.checkscategorias = [];
    $scope.textcategorias=[];
    $scope.paciente = {};
    $scope.cambiarObservacion("");
    $scope.tiposEntrega.forEach(function(tipo){
      tipo.selected=false;
    });

  };

  //abre una nueva ventana para crear paciente panoramax
  $scope.CheckPaciente = function(){
    $scope.nuevopaciente = {};
    $scope.modalNvaPaciente.show();
  }

  $scope.CheckAddress = function(direccionPedido) {
    $scope.direccionPedido = direccionPedido;
    console.log($scope.direccionPedido);

    if ($scope.direccionPedido === 'nva') {
      $scope.direccion = {};
      $scope.modalNvaDir.show();
    };
  };

  $scope.cerrarNvaDireccion = function() {
    $scope.direccionPedido = null;
    $scope.modalNvaDir.hide();
  };

  $scope.cerrarNvaPaciente = function() {
    $scope.modalNvaPaciente.hide();
  };

  //guarda paciente panoramax
  $scope.nvaPaciente = function(){
    var pacienteId = null;

    //almacenar paciente panoramax
    SignUpFactory
      .InsertPaciente($scope.paciente)
      .then(function(result) {
          $scope.paciente = result.data;
          $scope.cerrarNvaPaciente();
      })
      .catch(function(error){
        console.log(error);
      });
  }

  $scope.nvaDireccion = function() {
    var direccionId = null;
    $scope.direccion.DoctorId = $scope.profileData.Id;
    console.log("**************");
    console.log($scope.profileData.Id);
    console.log("**************");

    SignUpFactory
      .InsertAddress($scope.direccion)
      .then(function(result) {
        direccionId = result.data.Id;

        console.log(direccionId);

        $scope.profileData.Direccion  = [];

        return SignUpFactory
          .LoadAddress($scope.profileData.Id);
      })
      .then(function(result) {
        $scope.direcciones = result.data;
        $scope.profileData            = $scope.$parent.user;
        $scope.direccion              = {};

        $scope.cerrarNvaDireccion();
        $scope.direccionPedido = direccionId;
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  //funcion para agregar las subcategorias de panoramax
  $scope.agregarAPedido = function(){
    console.log("---------------------------"+$scope.checkscategorias.length);
    [].forEach.call($scope.checkscategorias,function(sub) {
      console.log(sub);
    });
    $scope.modal.hide();
  };

 //adiciona subcateogirias al array del pedido panoramax
  $scope.adicionarArraySubsChecks = function(sub, categorianombre){
    var subCatIdex = $scope.checkscategorias
      .map(function(sb) {
        return sb.Id;
      })
      .indexOf(sub.Id);

    sub.categorianombre = categorianombre;
    sub.cnt=1;

    if (subCatIdex < 0) {
      $scope.checkscategorias.push(sub);
    } else {
      $scope.checkscategorias.splice(subCatIdex, 1);
    }
    console.log("subbb----",sub);
    console.log($scope.checkscategorias);
  }

  $scope.seleccionarCategoria = function(item) {
    var subcategorias = item.SubCategoria.filter(function(sub) {
      return sub.campo == 'none';
    }).forEach(function(sub) {
      $scope.adicionarArraySubsChecks(sub, item.nombre);
    });
  }

  $scope.mostrarClase = function(item, $index) {
    var condicion = !!item.clase;

    if (condicion && $index > 0) {
      var itemAnt = $scope.categoriasbytiposervicio[$index-1];

      condicion = condicion && item.clase != itemAnt.clase;
    }

    item.mostrarClase = condicion;

    $scope.mostrarClaseSub(item.SubCategoria);
  }

  $scope.mostrarClaseSub = function(options) {
    options.forEach(function(item, $index) {
      var condicion = !!item.clase;

      if (condicion && $index > 0) {
        var itemAnt = options[$index-1];

        condicion = condicion && item.clase != itemAnt.clase;
      }

      item.mostrarClase = condicion;
    });
  }

  // panoramax busca un texto para actualizarlo en la lista de subcategorias
  //en los campos que se escribe
  function buscar(texto,sub_encontrar){
      var ban = false;
      [].forEach.call($scope.textcategorias,function(sub) {
        if(sub_encontrar.Id==sub.Id ){
          if(sub_encontrar.textocategoria!=texto){
            sub.textocategoria=texto;
          }
          ban=true;
        }
      });
      return ban;
  }

  $scope.adicionarArraySubsText = function(texto,sub,categorianombre){
    var pos = $scope.textcategorias.map(function(item) { return item.Id }).indexOf(sub.Id);

    console.log(pos, sub.textocategoria);

    if(sub.textocategoria != ""){
      sub.cnt=1;
      sub.textocategoria = texto;
      sub.categorianombre = categorianombre;
      if (pos < 0) {
        console.log('add');
        $scope.textcategorias.push(sub);
      } else {
        console.log('update');
        $scope.textcategorias[pos] = sub;
      }
    } else if (pos >= 0) {
      $scope.textcategorias.splice(pos, 1);

      console.log('delete');
    }

    console.log($scope.textcategorias);
  }

  $scope.GuardarPedido = function() {
    var pedido    = {};
    var direccion = [].find.call($scope.profileData.Direccion,
      function(dir) {
        return dir.Id == $scope.direccionPedido;
      });

    if (!direccion) {
      $ionicLoading.show({
        template: [
          '<i class="icon ion-alert-circled" style="font-size:3rem"></i>',
          '',
          'Antes de enviar tu pedido',
          'debes de seleccionar una dirección valida'
        ].join('<br>')
      });

      $timeout(function() {
        $ionicLoading.hide();
      }, 2000);

      return;
    };

    if (!$scope.medioPago) {
      $ionicLoading.show({
        template: [
          '<i class="icon ion-alert-circled" style="font-size:3rem"></i>',
          '',
          'Antes de enviar tu pedido',
          'debes de seleccionar un medio de pago valido'
        ].join('<br>')
      });

      $timeout(function() {
        $ionicLoading.hide();
      }, 2000);

      return;
    };

    $ionicLoading.show({
      template: [
        '<ion-spinner></ion-spinner>',
        'Espera un momento,',
        'estamos guardando tu pedido...'
      ].join('<br>')
    });

    pedido.Id                   = 0;
    pedido.Fecha                = new Date();
    pedido.Estado               = 'A';
    pedido.UsuarioTelefono      = $scope.profileData.Telefono;
    pedido.DireccionTipo        = direccion.Tipo;
    pedido.DireccionNum1        = direccion.Num1;
    pedido.DireccionNum2        = direccion.Num2;
    pedido.DireccionNum3        = direccion.Num3;
    pedido.DireccionDescripcion = direccion.Descripcion;
    pedido.DetallePedido        = [];
    pedido.MedioPagoId          = $scope.medioPago;

    [].forEach.call(CestaFactory.GetItems(),function(pro) {
      if (pro.cnt) {
        pedido.DetallePedido.push({
          Cantidad: pro.cnt,
          PorcentajePromo: 0,
          Precio: pro.Precio,
          ProductoId: pro.Id
        });
      }
    });

    if (pedido.DetallePedido.length == 0) {
      $ionicLoading.show({
        template: [
          '<i class="icon ion-alert-circled" style="font-size:3rem"></i>',
          '',
          'Antes de enviar tu pedido',
          'debes de agregar algunos productos a tu cesta'
        ].join('<br>')
      });

      $timeout(function() {
        $ionicLoading.hide();
      }, 2000);

      return;
    };

    ProductFactory
      .InsertPedido(pedido)
      .then(function(result) {
        $timeout(function() {
          $ionicLoading.show({
            template: [
              'Su pedido fue guardado exitosamente,',
              'en breve estaremos entregando tu pedido...'
            ].join('<br>')
          });
          $scope.VaciarCanasta();
        }, 1000)
        .then(function() {
          $timeout(function() {
            $ionicLoading.hide();
          }, 1000);
        });

      })
      .catch(function(error) {
        $ionicLoading.hide();
      });
  };

  $scope.cambiarObservacion = function(observacion) {
    $scope.observaciones = observacion;
  };

  //funcion para enviar pedido a panoramax
  $scope.EnviarPedidoPanoramax = function() {

    console.log("direccionpedido:"+$scope.direccionPedido);
    console.log("pacienteId"+$scope.paciente.Id);
    if($scope.direccionPedido == undefined){
      ionicToast.show('Por favor seleccione una dirección.', 'bottom', false, 2500);
    }else if($scope.paciente.Id == undefined){
      ionicToast.show('Por favor crear un Paciente primero.', 'bottom', false, 2500);
    }else if($scope.productoSeleccionado == undefined){
      ionicToast.show('Por favor selecciona un tipo de servicio.', 'bottom', false, 2500);
    }else if($scope.checkscategorias.length==0){
      ionicToast.show('Por favor seleccione productos.', 'bottom', false, 2500);
    }else{

        $ionicLoading.show({
          template: [
            'Se está enviando su orden,'
          ].join('<br>')
        });

        var usr = $auth.getPayload().sub;

        var data = [];

        //checks
        [].forEach.call($scope.checkscategorias,function(sub) {

          var detalle = {
            "campoAbierto" : "ninguno",
            "SubCategoriumId": sub.Id,
            "SubCategoriumNombre":sub.nombre,
            "CategoriaId":sub.CategoriumId,
            "CategoriaNombre":sub.categorianombre
          };
          console.log(detalle);
          data.push(detalle);
        });
        //text
        [].forEach.call($scope.textcategorias,function(sub) {

          var detalle = {
            "campoAbierto" : sub.textocategoria,
            "SubCategoriumId": sub.Id,
            "SubCategoriumNombre":sub.nombre,
            "CategoriaId":sub.CategoriumId,
            "CategoriaNombre":sub.categorianombre
          };
          console.log(detalle);
          data.push(detalle);
        });


        var pedido = {
          "DoctorNombre": usr.nombres+" "+usr.apellidos,
          "DoctorId" : usr.Id,
          "PacienteId": $scope.paciente.Id,
          "PacienteNombre":  $scope.paciente.nombres+" "+ $scope.paciente.apellidos,
          "observaciones": $scope.observaciones,
          "TipoServicioId": $scope.productoSeleccionado.Id,
          "TipoEstadoId": 1,
          "TipoServicioNombre": $scope.productoSeleccionado.nombre,
          "EmpresaId": parseInt($scope.direccionPedido)
        }

        var pedidocabecera = {
          "pedido": pedido,
          "detalles": data
        }

        console.log(pedido);
        console.log(data);
        console.log(pedidocabecera);

        CategoriaFactory
          .InsertPedidoPanoramax(pedidocabecera)
          .then(function(result){
            var pedidoResult = result;

            console.log("pedido insertado"+ result.data);
            var data = [];
            //checks
            [].forEach.call($scope.checkscategorias,function(sub) {

              var detalle = {
                "campoAbierto" : "ninguno",
                "SubCategoriumId": sub.Id,
                "SolicitudExameneId":result.data.Id,
              };
              console.log(detalle);
              data.push(detalle);
            });
            //text
            [].forEach.call($scope.textcategorias,function(sub) {

              var detalle = {
                "campoAbierto" : sub.textocategoria,
                "SubCategoriumId": sub.Id,
                "SolicitudExameneId":pedidoResult.data.Id,
              };
              console.log(detalle);
              data.push(detalle);
            });


            CategoriaFactory
              .InsertDetallePedidoPanoramax(data)
              .then(function(result){
                var data2 = [];

                $scope.tiposEntrega
                  .filter(function(tipo) { return tipo.selected == true; })
                  .forEach(function(tipo) {
                    data2.push({
                      SolicitudExameneId: pedidoResult.data.Id,
                      TipoEntregaResultadoId: tipo.Id
                    })
                  });

                return TipoEntregaResultadosFactory.InsertarSolicitudTipoEntrega(data2);
              })
              .then(function(result) {
                $scope.VaciarPedidoPanoramax();
                $ionicLoading.hide();
                ionicToast.show('Orden enviada correctamente.', 'bottom', false, 4000);
              })
              .catch(function(error){
                console.log(error);
                $ionicLoading.hide();
              });
          })
          .catch(function(error){
            console.log(error);
            $ionicLoading.hide();
          });
    }
  }


}
