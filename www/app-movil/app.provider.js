angular.module('urlBase', [])
  .provider('$urlBaseApi', [function() {
    var pro = this;

    pro.Url = function() {
      var localAPI  = 'http://192.168.0.15:3008/api/v00';
      //var remoteAPI = 'http://104.236.103.170:3001/api/v00';
      var remoteAPI = 'http://www.masdomicilios.club:3005/api/v00';

      return remoteAPI;
      //return window.location.hostname == 'localhost' ? localAPI : remoteAPI;
    };

    this.$get = [function() {
      var api = {};

      api.Url = pro.Url;

      return api;
    }];
  }]);
