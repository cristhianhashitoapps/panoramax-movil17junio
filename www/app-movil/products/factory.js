angular
  .module('app-movil')
  .factory('ProductFactory', ProductFactory)
  .factory('MedioPagoFactory', MedioPagoFactory)
  .factory('ParametroFactory', ParametroFactory)
  .factory('TipoEntregaResultadosFactory', TipoEntregaResultadosFactory);

ProductFactory.$inject = ['$http', '$q', '$urlBaseApi'];
function ProductFactory($http, $q, $url) {
  var urlBase = $url.Url();
  var api = {};

  api.GetProducts = function() {
    // return $q(function(resolve) {
    //   resolve({data: []});
    // });
    return $http.get([urlBase, 'productos'].join('/'));
  };

  api.GetProductsByVariedad = function() {
    // return $q(function(resolve) {
    //   resolve({data: []});
    // });
    return $http.get([urlBase, 'variedades'].join('/'));
  };

  api.InsertPedido = function(pedido) {
    // return $q(function(resolve) {
    //   resolve({data: []});
    // });
    return $http.post([urlBase, 'pedidos'].join('/'), pedido);
  };

  api.UpdatePedido = function(Id, pedido) {
    return $http.put([urlBase, 'pedidos', Id].join('/'), pedido);
  };

  return api;
}

MedioPagoFactory.$inject = ['$http', '$urlBaseApi'];
function MedioPagoFactory($http, $url) {
  var api     = {};
  var urlBase = $url.Url();

  api.GetAllMedioPago = function() {
    return $http.get([urlBase, 'medios-pago'].join('/'));
  };

  return api;
}

TipoEntregaResultadosFactory.$inject = ['$http', '$urlBaseApi'];
function TipoEntregaResultadosFactory($http, $url) {
  var api     = {};
  var urlBase = $url.Url();

  api.GetAllTipoEntregaResultados = function() {
    return $http.get([urlBase, 'TipoEntregaResultados'].join('/'));
  };

  api.InsertarSolicitudTipoEntrega = function(data) {
    return $http.post([urlBase, 'SolicitudTipoEntrega'].join('/'), data);
  }
  return api;
}

ParametroFactory.$inject = ['$http', '$urlBaseApi'];
function ParametroFactory($http, $url) {
  var api     = {};
  var urlBase = $url.Url();

  api.GetValorDomicilio = function() {
    return $http.get([urlBase, 'parametros', 1].join('/'));
  };

  return api;
}
