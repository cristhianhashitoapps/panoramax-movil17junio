angular
  .module('app-movil')
  .controller('AppCtrl', [
    '$scope',
    '$state',
    '$ionicModal',
    '$timeout',
    '$auth',
    'SignUpFactory',
    function($scope, $state, $ionicModal, $timeout, $auth, SignUpFactory) {
      $scope.user     = $auth.getPayload().sub;
      $scope.userMd5  = hex_md5($scope.user.correo.toLowerCase());
      $scope.products = [];

      function activate() {
        SignUpFactory
          .LoadAddress($scope.user.telefono)
          .then(function(result) {
            $scope.user.Direccion = result.data;
          })
          .catch(function(error) {
            console.log(error);
          });
      }

      activate();

      $scope.showOrders = function() {
        $state.go('app.orders', {}, {reload: true});
      };

      $scope.selectCategory = function() {
        $state.go('app.category', {}, {reload: true});
      };
    }]);
