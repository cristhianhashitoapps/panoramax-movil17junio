angular
  .module('app-movil')
  .factory('CestaFactory', CestaFactory);

CestaFactory.$inject = [];
function CestaFactory() {
  var items       = [];
  var dataFactory = {};

  dataFactory.GetItems = function() {
    return items;
  };

  dataFactory.ContainsProduct = function(product) {
    var pro = items.find(function(item) {
      return item.Id == product.Id;
    });

    return pro;
  };

  dataFactory.AddProduct = function(product, cant) {
    var prod = dataFactory.ContainsProduct(product);

    if (!prod) {
      prod     = JSON.parse(JSON.stringify(product));
      prod.cnt = 0;
      items.push(prod);
    }

    prod.cnt += cant;
  };

  dataFactory.Clear = function() {
    items = [];
  };

  return dataFactory;
}
